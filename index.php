<!DOCTYPE html>
<html lang="it">

<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-165946727-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }

        gtag('js', new Date());

        gtag('config', 'UA-165946727-1');
    </script>

    <meta charset="utf-8"/>
    <meta http-equiv="x-ua-compatible" content="ie=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no"/>
    <meta name="keywords" content="Videochat, semplice, free, no registrazione, sicura, anonima, privacy">
    <meta name="author" content="Diego Sinitò">
    <meta name="description" content="È semplicemente una videochat. Nessuna registrazione, nessun costo, nessun download.
    La chiamata è anonima, il traffico è scambiato direttamente tra te e il tuo contatto."/>

    <title>It's a videochat: semplice, anomima, gratuita.</title>

    <link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/8.0.1/normalize.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.1/css/all.min.css">
    <link rel="stylesheet" type="text/css" href="css/main.css?v=<?php echo time(); ?>">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.min.css">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://webrtc.github.io/adapter/adapter-latest.js"></script>
    <script src="https://unpkg.com/peerjs@1.1.0/dist/peerjs.min.js"></script>
</head>

<body>
<div class="popup">
    <button id="close-popup"><i class="fas fa-times"></i></button>
    <video id="popup-stream" autoplay></video>
</div>
<div class="left-wrapper">
    <div class="header">
        <i class="fas fa-camera" style="margin-right: 10px"></i> It's a videochat

        <i class="chat-button far fa-comments" onclick="slideIn()">
            <span class="notification"></span>
        </i>

    </div>
    <div class="video-wrapper">
        <video id="other-stream" class="other-stream" autoplay></video>
        <video id="my-stream" class="my-stream reverse" muted="muted" autoplay></video>
        <div class="call-button-wrapper">
            <button id="call-close" class="controll-button red">
                <i class="fas fa-phone"
                   style="margin-top: 3px; -webkit-transform: rotate(-136deg); transform: rotate(-136deg);"></i>
            </button>
            <button id="swap-camera" class="controll-button"><i class="fas fa-sync-alt"></i></button>
            <button id="screen-share" class="controll-button"><i class="far fa-clone"></i></button>
            <button id="full-screen" class="controll-button"><i class="fas fa-expand-arrows-alt"></i></button>
        </div>
    </div>
    <div class="footer">
        <div class="navbar">
            <span id="title">videochiamata</span>
            <div class="button-wrapper">
                <button class="section-selector selected" section="videochiamata">videochiamata</button>
                <button class="section-selector" section="crediti">info</button>
            </div>
        </div>
        <div id="videochiamata" class="content">
            <div class="box-wrapper" style="flex-grow: 3">
                <h3>Chiama..</h3>
                <p>Inserisci l'<b>ID</b> della persona che vuoi chiamare e avvia la chiamata.</p>
                <div style="display: flex">
                    <input id="other-peerID" type="text" placeholder="inserisci l'ID..." value="<?php echo isset($_GET['id']) ? $_GET['id'] : ''; ?>">
                    <button id="call-button"><i class="fas fa-phone"></i></button>
                </div>
            </div>

            <div class="box-wrapper" style="flex-grow: 3">
                <h3>..altrimenti fatti chiamare</h3>
                <p>Questo è il tuo ID (<b id="peerID" style="text-transform: uppercase">non sei connesso :(</b>),
                    comunicalo alla persona con cui vuoi connetterti. Clicca per copiare il link diretto.</p>
                <p id="copy">
                    <i class="far fa-copy" style="font-size: 1.5em"></i>
                    <input id="toCopy" type="text" style="width: 86%; border: 0; cursor: pointer;" readonly>
                </p>
            </div>
        </div>
        <div id="crediti" class="content" style="display: none">
            <div class="box-wrapper" style="flex-grow: 2">
                <h2>Cos'è?</h2>
                <p>È <b>semplicemente</b> una videochat.<br>
                    Nessuna registrazione, nessun costo, nessun download. Chiama qualcuno usando il suo ID o comunica il
                    tuo ID e fatti chiamare.
                    La chiamata è <b>anonima</b>, il traffico è scambiato direttamente tra te e il tuo contatto.</p>
            </div>

            <div class="box-wrapper">
                <h2>Chi l'ha fatta?</h2>
                <p>L'ho fatta <b><a href="https://www.facebook.com/sinito.diego" target="_blank">io</a></b>.<br>
                    Giorno 01/05/2020 mi sono svegliato alle 7:00, ero annoiato e non avevo nulla di meglio da fare.</p>
            </div>

            <div class="box-wrapper">
                <h2>Com'è fatta?</h2>
                <p>Il codice è scritto in HTML e JavaScript. La connessione avviene per mezzo della libreria PeerJS,
                    basata su WebRTC.</p>
            </div>
        </div>


    </div>
</div>

<div class="right-wrapper">
    <div class="header">
        <label>Il tuo nome:</label>
        <input id="nome" type="text" placeholder="inserisci il tuo nome...">

        <i class="chat-button fas fa-times" onclick="slideOut()"></i>
    </div>

    <div id="message-wrapper" class="messagge-wrapper">
    </div>

    <div class="footer">
        <label for="file" class="fas fa-paperclip"></label>
        <input id="file" type="file" style="display: none;">
        <div contenteditable="false" id="message" placeholder="Scrivi qui...">Prima di usare la chat dovresti collegerti
            con qualcuno e scegliere un nome (se vuoi)...
        </div>
        <button id="send-button"><i class="fas fa-paper-plane"></i></button>
    </div>

</div>
</body>

<script>
    window.addEventListener("beforeunload", function (e) {
        (e || window.event).returnValue = null;
        return null;
    });

    $('.notification').hide();
    $('.controll-button').hide();

    function slideIn() {
        $('.right-wrapper').animate({
            right: '0'
        }, 100);
    }

    function slideOut() {
        notification = null;
        $('.notification').text('');
        $('.notification').hide();

        $('.right-wrapper').animate({
            right: '-100%'
        }, 100);
    }

    function mostraControlli() {
        $('.call-button-wrapper .controll-button').fadeIn();

        setTimeout(function () {
            $('.call-button-wrapper .controll-button').fadeOut();
        }, 5000);
    }

    $('#full-screen').on('click', function () {
        var $otherStream = document.querySelector('#other-stream');
        var $popupStream = document.querySelector('#popup-stream');
        $popupStream.srcObject = otherStream;
        $otherStream.srcObject = null;

        popupOn = true;

        $('.popup').toggle();
    });

    $('#close-popup').on('click', function () {
        var $otherStream = document.querySelector('#other-stream');
        var $popupStream = document.querySelector('#popup-stream');
        $otherStream.srcObject = otherStream;
        $popupStream.srcObject = null;

        popupOn = false;

        $('.popup').toggle();
    });

    $('.section-selector').on('click', function () {
        let section = $(this).attr('section');

        $('.section-selector').removeClass('selected');
        $(this).addClass('selected');

        $('#title').text($(this).text());

        $('.footer .content').hide();
        $('#' + section).show();
    });

    $('#copy').on('click', function () {
        var copyText = document.getElementById('toCopy');

        copyText.select();
        copyText.setSelectionRange(0, 99999);

        document.execCommand('copy');

        alert('Link copiato! Condividilo e fatti chiamare...');
    });
</script>

<script>
    var myStream = null;
    var otherStream = null;
    var screenStream = null;
    var facingMode = 'user';

    async function startVideo(facingMode) {
        if (navigator.mediaDevices.getUserMedia) {
            try {
                myStream = await navigator.mediaDevices.getUserMedia({
                    video: {
                        facingMode: facingMode
                    },
                    audio: true
                });

                var video = document.querySelector('#my-stream');
                video.setAttribute('autoplay', '');
                video.setAttribute('muted', '');
                video.setAttribute('playsinline', '');
                video.srcObject = myStream;
                $('#my-stream').show();
            } catch (err) {
                alert("Ops, si è verificato un errore! (" + err + ")");
                return false;
            }
        } else {
            alert("C'è un problema con il tuo video! Bhè, dovresti risolverlo prima di usare una videochat...");
            return false;
        }

        return true;
    }

    async function startCapture() {
        try {
            screenStream = await navigator.mediaDevices.getDisplayMedia();

            var audioTracks = myStream.getAudioTracks();

            audioTracks.forEach(function (track) {
                screenStream.addTrack(track.clone());
            });
        } catch (err) {
            alert("Ops, si è verificato un errore! (" + err + ")");
            return false;
        }

        return true;
    }

    startVideo('user');

    var peer = null;
    var peerID = null;
    var otherPeer = null;
    var callObj = null;
    var connObj = null;
    var option = {
        host: 'peerjs-server.herokuapp.com',
        secure: true,
        port: 443,
        debug: 3
    };
    var notification = null;
    var captureOn = false;
    var popupOn = false;

    function sendMessage() {
        if (connObj == null)
            return false;

        var nome = $('#nome').val().split(' ');
        var iniziali = '';
        nome.forEach(function (value) {
            iniziali += value.charAt(0);
        });
        iniziali = iniziali.substr(0, 2);

        var tu = iniziali == "" ? "tu" : iniziali;
        var lui = iniziali == "" ? "lui" : iniziali;

        var message = $.trim($('#message').html());

        if (message == "")
            return;

        var data = {
            type: 'message',
            payload: '<div class="message other slide-in-right"><div class="icon">' + lui + '</div><div class="text"><p>' + message + '</p></div></div>'
        };

        connObj.send(data);
        $('#message-wrapper').append('<div class="message self slide-in-left"><div class="icon">' + tu + '</div><div class="text"><span>' + message + '</span></div></div>');

        setTimeout(function () {
            document.getElementById('message-wrapper').scrollTop = document.getElementById('message-wrapper').scrollHeight;
        }, 100);

        $('#message').html('');
        $('#message').focus();

        return true;
    }

    function getMessage(data) {
        if (notification == null)
            notification = 1;
        else
            notification++;
        $('.notification').text(notification);
        $('.notification').show();

        switch (data.type) {
            case 'message':
                $('#message-wrapper').append(data.payload);
                break;

            case 'attach':
                var downloadFile = new File([data.payload.data], data.payload.name);
                var url = URL.createObjectURL(downloadFile);

                switch (data.payload.type.split('/')[0]) {
                    case 'image':
                        $('#message-wrapper').append('<div class="message other slide-in-right">' +
                            '<div class="icon">' + data.payload.iniziali + '</div><div class="text"><span>' +
                            '<a href="' + url + '" download="' + data.payload.name + '">' +
                            '<img src="' + url + '" class="thumbnail"></a></span></div></div>');
                        break;

                    default:
                        $('#message-wrapper').append('<div class="message other slide-in-right">' +
                            '<div class="icon">' + data.payload.iniziali + '</div><div class="text"><span>' +
                            '<a href="' + url + '" download="' + data.payload.name + '">' + data.payload.name + '</a></span></div></div>');
                        break;
                }
                break;

            case 'comand':
                switch (data.payload) {
                    case 'close':
                        closeCall();
                        break;
                }
                break;
        }

        setTimeout(function () {
            document.getElementById('message-wrapper').scrollTop = document.getElementById('message-wrapper').scrollHeight;
        }, 100);
    }

    function connessione(id) {
        if (id == null) {
            peer = new Peer(option);
        } else {
            peer = new Peer(id, option);
        }

        peer.on('disconnected', function () {
            connessione(peerID);
        });

        peer.on('open', function (id) {
            $('#peerID').text(id);
            $('#toCopy').val('https://itsavideochat.it?id=' + id);
            peerID = id;
        });

        peer.on('call', function (call) {
            callObj = call;

            otherPeer = callObj.peer;
            $('#other-peerID').val(otherPeer);

            callObj.answer(myStream);

            callObj.on('stream', function (stream) {
                otherStream = stream;

                if (!popupOn)
                    var targetElement = document.querySelector('#other-stream');
                else
                    var targetElement = document.querySelector('#popup-stream');

                targetElement.setAttribute('autoplay', '');
                targetElement.setAttribute('playsinline', '');
                targetElement.srcObject = stream;

                $('.call-button-wrapper').on('mouseenter', function () {
                    mostraControlli();
                });
                $('#other-stream').show();
                mostraControlli();
            });

            callObj.on('close', function () {
                $('.call-button-wrapper').unbind();
            });

            callObj.on('error', function (err) {
                closeCall();
                alert("Ops, si è verificato un errore! (" + err + ")");
            });
        });

        peer.on('connection', function (conn) {
            connObj = conn;
            $('#message').html('');
            $('#message').attr('contenteditable', true);

            connObj.on('data', function (data) {
                getMessage(data);
            });

            connObj.on('error', function (err) {
                closeCall();
                alert("Ops, si è verificato un errore! (" + err + ")");
            });
        });

        peer.on('error', function (err) {
            if (err.type == 'network')
                return;

            alert("Ops, si è verificato un errore! (" + err.type + ")");
        });
    }

    function makeCall(peerId, videoStream) {
        callObj = peer.call(peerId, videoStream);
        connObj = peer.connect(peerId);

        callObj.on('stream', function (stream) {
            otherStream = stream;

            if (!popupOn)
                var targetElement = document.querySelector('#other-stream');
            else
                var targetElement = document.querySelector('#popup-stream');

            targetElement.setAttribute('autoplay', '');
            targetElement.setAttribute('playsinline', '');
            targetElement.srcObject = stream;

            $('.call-button-wrapper').on('mouseenter', function () {
                mostraControlli();
            });
            $('#other-stream').show();
            mostraControlli();
        });

        callObj.on('error', function (err) {
            closeCall();
            alert("Ops, si è verificato un errore! (" + err + ")");
        });

        connObj.on('open', function () {
            $('#message').html('');
            $('#message').attr('contenteditable', true);

            connObj.on('data', function (data) {
                getMessage(data);
            });
        });

        connObj.on('error', function (err) {
            closeCall();
            alert("Ops, si è verificato un errore! (" + err + ")");
        });
    }

    function closeCall() {
        if (callObj == null && connObj == null)
            return false;

        callObj.close();
        connObj.close();

        callObj = null;
        connObj = null;

        if (captureOn) {
            var tracks = screenStream.getTracks();
            tracks.forEach(track => track.stop());
            $(this).toggleClass('red');
            captureOn = false;
        }

        notification = null;
        $('.notification').text('');
        $('.notification').hide();
        $('.controll-button').hide();
        $('.call-button-wrapper').unbind();
        $('#message').html("Prima di usare la chat dovresti collegerti con qualcuno e scegliere un nome (se vuoi)...");
        $('#message').attr('contenteditable', false);
        $('#message-wrapper').html('');

        return true;
    }

    connessione(null);

    $('#call-button').click(function () {
        otherPeer = $('#other-peerID').val().toLowerCase();

        makeCall(otherPeer, myStream);
    });

    $('#swap-camera').on('click', function () {
        if (facingMode == 'user')
            facingMode = 'environment';
        else
            facingMode = 'user';

        Promise.resolve(startVideo(facingMode)).then(function (value) {
            if(value){
                makeCall(otherPeer, myStream);
            }
        });
    });

    $('#screen-share').on('click', function () {
        if (!captureOn) {
            Promise.resolve(startCapture()).then(function (value) {
                if(value){
                    $('#screen-share').toggleClass('red');
                    captureOn = true;

                    makeCall(otherPeer, screenStream);
                }
            });
        } else {
            makeCall(otherPeer, myStream);

            var tracks = screenStream.getTracks();
            tracks.forEach(track => track.stop());

            $('#screen-share').toggleClass('red');
            captureOn = false;
        }
    });

    $('#send-button').on('click', function (e) {
        sendMessage();
    });

    $('#message').on('keydown', function (e) {
        if (e.keyCode == 13 && e.shiftKey) {
            document.execCommand('insertHTML', false, '<br><br>');
            return false;
        }
        if (e.keyCode == 13 && !e.shiftKey) {
            e.preventDefault();
            sendMessage();

            return false;
        }
    });

    $('#file').on('change', function () {
        if (connObj == null)
            return;

        var file = document.getElementById("file");

        if (file.files[0].size / 1024 / 1024 > 10) {
            alert("Questo file è troppo grande, puoi inviare al massimo 10MB...");
            return;
        }

        var nome = $('#nome').val().split(' ');
        var iniziali = '';
        nome.forEach(function (value) {
            iniziali += value.charAt(0);
        });
        iniziali = iniziali.substr(0, 2);

        var tu = iniziali == "" ? "tu" : iniziali;
        var lui = iniziali == "" ? "lui" : iniziali;

        var dataObj = {
            name: file.files[0].name,
            type: file.files[0].type,
            data: file.files[0],
            iniziali: lui
        };

        var data = {
            type: 'attach',
            payload: dataObj
        };

        connObj.send(data);

        var url = URL.createObjectURL(file.files[0]);

        switch (file.files[0].type.split('/')[0]) {
            case 'image':
                $('#message-wrapper').append('<div class="message self slide-in-left">' +
                    '<div class="icon">' + tu + '</div><div class="text"><p>' +
                    '<a href="' + url + '" download="' + dataObj.name + '">' +
                    '<img src="' + url + '" class="thumbnail"></a></p></div></div>');
                break;

            default:
                $('#message-wrapper').append('<div class="message self slide-in-left">' +
                    '<div class="icon">' + tu + '</div><div class="text"><p>' +
                    '<a href="' + url + '" download="' + dataObj.name + '">' + dataObj.name + '</a></p></div></div>');
                break;
        }

        setTimeout(function () {
            document.getElementById('message-wrapper').scrollTop = document.getElementById('message-wrapper').scrollHeight;
        }, 100);

        $('#message').html('');
        $('#message').focus();
    });

    $('#call-close').on('click', function () {
        if (callObj == null && connObj == null)
            return false;

        var data = {
            type: 'comand',
            payload: 'close'
        };
        connObj.send(data);

        setTimeout(function () {
            closeCall();
        }, 500);
    });
</script>

</html>
